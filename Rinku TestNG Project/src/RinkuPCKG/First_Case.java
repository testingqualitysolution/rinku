package RinkuPCKG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class First_Case {
	public WebDriver driver;
	 @BeforeTest
	  public void beforeTest() {
		 System.setProperty("webdriver.chrome.driver", "C:\\Users\\manis\\Desktop\\Selenium Webdriver Setup\\chromedriver.exe");					
		    driver = new ChromeDriver();
		    driver.manage().window().maximize();
		    driver.manage().window().maximize();
		    driver.get("http://automationpractice.com/index.php");

		    driver.findElement(By.xpath("//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a")).click();
			
	  }
	
	@Test (priority=0)
  public void BothValueBlank() throws InterruptedException {
		driver.findElement(By.xpath("//*[@id=\"SubmitLogin\"]/span")).click();
		Thread.sleep(5000);
		
		String actual_value = driver.findElement(By.xpath("//*[@id=\"center_column\"]/div[1]/ol/li")).getText();
		 String expected_value = "An email address required.";
		 Assert.assertEquals(actual_value, expected_value);
  }
 
	@Test (priority=1)
	  public void PasswordBlank() throws InterruptedException {
		driver.findElement(By.xpath("//*[@id=\"email\"]")).sendKeys("ankur1.manish@gmail.com");
		driver.findElement(By.xpath("//*[@id=\"SubmitLogin\"]/span")).click();
		Thread.sleep(5000);
		
		String actual_value = driver.findElement(By.xpath("//*[@id=\"center_column\"]/div[1]/ol/li")).getText();
		 String expected_value = "Password is required.";
		 Assert.assertEquals(actual_value, expected_value);
  
	  }
	
	

  @AfterTest
  public void afterTest() {
  }

}
